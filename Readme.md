# Jenkins demo project IaC files

This demo project includes Docker and Docker Compose files, establishing an infrastructure designed for swift deployment of a Jenkins instance alongside supplementary services for network access and monitoring. The Jenkins instance is pre-configured with several additional modules for enhanced functionality. All services, including Jenkins, are deployed as Docker containers. It is presumed that Jenkins has access to the docker.sock file, enabling it to execute Docker containers.`

## preinstalled plugins
* pipeline-model-definition 
* docker-plugin 
* docker-workflow 
* gitlab-plugin 
* gitlab-branch-source 
* metrics 
* prometheus 
* configuration-as-code 
* blueocean

## Topology

![](environment-diagram.png)